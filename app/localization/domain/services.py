import abc
from app.localization.domain.dtos import TranslateRequest


class LocalizationServiceInterface(abc.ABC):

    @abc.abstractmethod
    def translate(self, request: TranslateRequest) -> str:
        pass


class DummyLocalizationService(LocalizationServiceInterface):

    def translate(self, request: TranslateRequest) -> str:
        return request.text
