from dataclasses import dataclass
from .entities import Locale


@dataclass(frozen=True)
class TranslateRequest:
    text: str
    locale: Locale
