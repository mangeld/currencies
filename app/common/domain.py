import abc
from dataclasses import dataclass


@dataclass
class DomainError:
    code: int
    message: str
    slug: str


class Service(abc.ABC):
    pass
