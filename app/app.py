from flask import Flask

from app.currencies.framework.handlers import currencies_blueprint
from app.rates.framework.handlers import rates_blueprint

app = Flask(__name__)

app.register_blueprint(currencies_blueprint)
app.register_blueprint(rates_blueprint)


if __name__ == "__name__":
    app.run()
