from app.rates.domain.interfaces import RateServiceInterface
from app.rates.domain.services import RateService
from app.rates.framework.repositories import DummyRateRepository


def currency_rate_factory() -> RateServiceInterface:
    return RateService(DummyRateRepository())
