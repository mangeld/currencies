from app.rates.domain.services import RateService
from app.rates.domain.dtos import FilterRateRequest
from unittest.mock import MagicMock
import pytest


@pytest.mark.unit
class TestRateService:

    def test_filter_rates_calls_get_all(self):
        fake_rate_repo = MagicMock()
        service = RateService(fake_rate_repo)

        service.filter_rates(
            FilterRateRequest(from_currency=None, to_currency=None)
        )

        fake_rate_repo.get_all.assert_called_once()

    def test_filter_rates_calls_filter(self):
        fake_rate_repo = MagicMock()
        service = RateService(fake_rate_repo)

        service.filter_rates(
            FilterRateRequest(from_currency='eur', to_currency=None)
        )

        fake_rate_repo.filter.assert_called_once()

    def test_filter_rates_calls_get_rate(self):
        fake_rate_repo = MagicMock()
        service = RateService(fake_rate_repo)

        service.filter_rates(
            FilterRateRequest(from_currency='eur', to_currency='aud')
        )

        fake_rate_repo.get_rate.assert_called_once()
