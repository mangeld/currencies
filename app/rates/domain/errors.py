from app.common.domain import DomainError
from dataclasses import dataclass


@dataclass
class RateAlreadyExists(DomainError):
    code: int = 1000
    message: str = "Rate already exists"
    slug: str = "rate-exists"


@dataclass
class RateNotFound(DomainError):
    code: int = 1001
    message: str = "Rate not found"
    slug: str = "rate-not-found"
