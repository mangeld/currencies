from dataclasses import dataclass
from datetime import datetime
from typing import Optional


@dataclass
class CurrencyRate:
    origin_id: str
    destination_id: str
    rate: float
    last_updated: Optional[datetime] = None
