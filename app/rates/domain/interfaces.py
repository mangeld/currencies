import abc
from typing import List, Optional, Tuple

from app.common.domain import DomainError
from app.rates.domain.dtos import (CreateRateRequest, FilterRateRequest,
                                   UpdateRateRequest)
from app.rates.domain.entities import CurrencyRate


class RateRepositoryInterface(abc.ABC):

    @abc.abstractmethod
    def get_rate(self, from_id: str, to_id: str) -> Optional[CurrencyRate]:
        pass

    @abc.abstractmethod
    def filter(self, from_id: str) -> List[CurrencyRate]:
        pass

    @abc.abstractmethod
    def get_all(self) -> List[CurrencyRate]:
        pass

    @abc.abstractmethod
    def create(self,
               rate: CurrencyRate
               ) -> Tuple[Optional[CurrencyRate], List[DomainError]]:
        pass

    @abc.abstractmethod
    def update(self,
               rate: CurrencyRate
               ) -> Tuple[Optional[CurrencyRate], List[DomainError]]:
        pass


class RateServiceInterface(abc.ABC):

    @abc.abstractmethod
    def filter_rates(self, request: FilterRateRequest) -> List[CurrencyRate]:
        pass

    @abc.abstractmethod
    def create_rate(self,
                    request: CreateRateRequest
                    ) -> Tuple[Optional[CurrencyRate], List[DomainError]]:
        pass

    @abc.abstractmethod
    def update_rate(self,
                    request: UpdateRateRequest
                    ) -> Tuple[Optional[CurrencyRate], List[DomainError]]:
        pass
