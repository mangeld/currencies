from dataclasses import dataclass
from typing import Optional


@dataclass
class FilterRateRequest:
    from_currency: Optional[str] = None
    to_currency: Optional[str] = None


@dataclass
class CreateRateRequest:
    from_currency: str
    to_currency: str
    rate: float


@dataclass
class UpdateRateRequest(CreateRateRequest):
    pass
