from datetime import datetime
from typing import List, Optional, Tuple

from app.common.domain import DomainError
from app.rates.domain.dtos import (CreateRateRequest, FilterRateRequest,
                                   UpdateRateRequest)
from app.rates.domain.entities import CurrencyRate
from app.rates.domain.errors import RateAlreadyExists, RateNotFound
from app.rates.domain.interfaces import (RateRepositoryInterface,
                                         RateServiceInterface)


class RateService(RateServiceInterface):

    def __init__(self, rate_repo: RateRepositoryInterface) -> None:
        self.rate_repo = rate_repo

    def filter_rates(self, request: FilterRateRequest) -> List[CurrencyRate]:
        if request.from_currency and request.to_currency:
            rate = self.rate_repo.get_rate(
                request.from_currency,
                request.to_currency
            )
            return [rate] if rate else []

        if request.to_currency is None and request.from_currency is not None:
            return self.rate_repo.filter(request.from_currency)

        return self.rate_repo.get_all()

    def create_rate(self,
                    request: CreateRateRequest
                    ) -> Tuple[Optional[CurrencyRate], List[DomainError]]:
        rate = self.rate_repo.get_rate(
            request.from_currency,
            request.to_currency
        )
        if rate:
            return None, [RateAlreadyExists()]
        return self.rate_repo.create(CurrencyRate(
            origin_id=request.from_currency,
            destination_id=request.to_currency,
            rate=request.rate,
            last_updated=datetime.now(),
        ))

    def update_rate(self,
                    request: UpdateRateRequest
                    ) -> Tuple[Optional[CurrencyRate], List[DomainError]]:
        rate = self.rate_repo.get_rate(
            request.from_currency,
            request.to_currency
        )
        if not rate:
            return None, [RateNotFound()]
        return self.rate_repo.update(CurrencyRate(
            origin_id=request.from_currency,
            destination_id=request.to_currency,
            rate=request.rate,
            last_updated=datetime.now(),
        ))
