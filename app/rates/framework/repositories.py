from datetime import datetime
from itertools import chain
from typing import List, Optional, Tuple

from app.common.domain import DomainError
from app.rates.domain.entities import CurrencyRate
from app.rates.domain.interfaces import RateRepositoryInterface


class DummyRateRepository(RateRepositoryInterface):

    def __init__(self):
        self.rates = {
            'eur': [
                CurrencyRate('eur', 'usd', 1.11, datetime.now()),
                CurrencyRate('eur', 'gbp', 0.91, datetime.now()),
                CurrencyRate('eur', 'aud', 1.61, datetime.now()),
            ]
        }

    def get_rate(self, from_id: str, to_id: str) -> Optional[CurrencyRate]:
        from_currencies = self.rates[from_id]
        try:
            return next(filter(
                lambda e: e.destination_id == to_id,
                from_currencies
            ))
        except StopIteration:
            return None

    def filter(self, from_id: str) -> List[CurrencyRate]:
        return self.rates.get(from_id, [])

    def get_all(self) -> List[CurrencyRate]:
        return list(chain(*self.rates.values()))

    def create(self,
               rate: CurrencyRate
               ) -> Tuple[Optional[CurrencyRate], List[DomainError]]:
        return rate, []

    def update(self,
               rate: CurrencyRate
               ) -> Tuple[Optional[CurrencyRate], List[DomainError]]:
        return rate, []
