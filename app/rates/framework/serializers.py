from flask import url_for
from marshmallow import Schema, fields, post_load, pre_dump

from app.currencies.framework.serializers import CurrencyURL
from app.rates.domain.entities import CurrencyRate


class RateSchema(Schema):
    currencies = fields.List(fields.Nested(CurrencyURL))
    last_updated = fields.DateTime()
    rate = fields.Float()

    @pre_dump
    def map_currencies(self, data: CurrencyRate):
        def absolute_currency_link(currency_id: str) -> str:
            return url_for(
                'currencies.handle_get_currency',
                currency_code=currency_id,
                _external=True
            )

        data.currencies = [  # type: ignore
            {
                'id': data.origin_id,
                'link': absolute_currency_link(data.origin_id),
            },
            {
                'id': data.destination_id,
                'link': absolute_currency_link(data.destination_id),
            },
        ]


class ModifyRateSchema(Schema):
    from_currency = fields.Str()
    to_currency = fields.Str()
    rate = fields.Float()

    @post_load
    def to_domain(self, data, **kwargs) -> CurrencyRate:
        return CurrencyRate(
            origin_id=data.get('from_currency'),
            destination_id=data.get('to_currency'),
            rate=data.get('rate'),
        )
