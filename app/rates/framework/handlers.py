from flask import Blueprint, jsonify, request

from app.rates.domain.dtos import (CreateRateRequest, FilterRateRequest,
                                   UpdateRateRequest)
from app.rates.factories import currency_rate_factory
from app.rates.framework.serializers import ModifyRateSchema, RateSchema

rates_blueprint = Blueprint('rates', __name__)


@rates_blueprint.route('/rates/', methods=('GET',))
def handle_get_all_rates():
    currency_rate_service = currency_rate_factory()
    rates = currency_rate_service.filter_rates(
        FilterRateRequest(None, None)
    )
    return jsonify(RateSchema().dump(rates, many=True).data)


@rates_blueprint.route(
    '/rates/<from_currency>/<to_currency>/',
    methods=('GET',)
)
def handle_get_rate(from_currency, to_currency):
    currency_rate_service = currency_rate_factory()
    rates = currency_rate_service.filter_rates(
        FilterRateRequest(from_currency, to_currency)
    )

    return jsonify(RateSchema().dump(rates, many=True).data)


@rates_blueprint.route('/rates/', methods=('POST',))
def handle_create_rate():
    currency_rate_service = currency_rate_factory()
    result = ModifyRateSchema().load(request.json)
    rate, errors = result.data, result.errors

    if errors.keys():
        return jsonify(errors), 422

    rate, errors = currency_rate_service.create_rate(CreateRateRequest(
        from_currency=rate.origin_id,
        to_currency=rate.destination_id,
        rate=rate.rate
    ))

    if errors:
        return jsonify(errors), 422

    return jsonify(RateSchema().dump(rate).data), 201


@rates_blueprint.route(
    '/rates/<from_currency>/<to_currency>/',
    methods=('PUT', 'PATCH')
)
def handle_update_rate(from_currency, to_currency):
    currency_rate_service = currency_rate_factory()

    request_data = {
        'from_currency': from_currency,
        'to_currency': to_currency,
        **request.json
    }
    result = ModifyRateSchema().load(request_data)
    update_request, errors = result.data, result.errors

    if errors.keys():
        return jsonify(errors), 422

    rate, errors = currency_rate_service.update_rate(UpdateRateRequest(
        from_currency=update_request.origin_id,
        to_currency=update_request.destination_id,
        rate=update_request.rate,
    ))

    if errors:
        return jsonify(errors), 422

    return jsonify(RateSchema().dump(rate).data), 200
