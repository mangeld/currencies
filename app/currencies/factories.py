from app.currencies.domain.domain import CurrencyService, GetCurrencyUseCase
from app.currencies.framework.repositories import DummyCurrencyRepo
from app.localization.domain.services import DummyLocalizationService


def get_currency_uc_factory() -> GetCurrencyUseCase:
    return GetCurrencyUseCase(
        CurrencyService(DummyCurrencyRepo()),
        DummyLocalizationService()
    )


def currency_service_factory() -> CurrencyService:
    return CurrencyService(DummyCurrencyRepo())
