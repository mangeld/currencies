import abc
from dataclasses import dataclass
from typing import List, Optional, Tuple

from app.common.domain import DomainError
from app.currencies.domain.entities import Currency


@dataclass
class CurrencyNotFound(DomainError):
    code: int = 1000
    message: str = "Currency not found"
    slug: str = "Currency not found"


class CurrencyRepo(abc.ABC):

    @abc.abstractmethod
    def get(self, key: str) -> Tuple[Optional[Currency], List[DomainError]]:
        pass

    @abc.abstractmethod
    def get_all(self) -> List[Currency]:
        pass


class CurrencyServiceInterface(abc.ABC):

    @abc.abstractmethod
    def get_currency(self,
                     key: str
                     ) -> Tuple[Optional[Currency], List[DomainError]]:
        pass

    @abc.abstractmethod
    def get_all_currencies(self) -> List[Currency]:
        pass
