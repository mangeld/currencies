from typing import List, Optional, Tuple

from app.common.domain import DomainError
from app.currencies.domain.entities import Currency
from app.currencies.domain.interfaces import (CurrencyRepo,
                                              CurrencyServiceInterface)
from app.localization.domain.dtos import TranslateRequest
from app.localization.domain.entities import Locale
from app.localization.domain.services import LocalizationServiceInterface


class GetCurrencyUseCase:

    def __init__(self,
                 currency_service: CurrencyServiceInterface,
                 localization_service: LocalizationServiceInterface) -> None:
        self.currency_service = currency_service
        self.localization_service = localization_service

    def execute(self,
                key: str,
                locale: Optional[Locale] = None
                ) -> Tuple[Optional[Currency], List[DomainError]]:
        currency, errors = self.currency_service.get_currency(key)
        if locale and currency:
            currency.name = self.localization_service.translate(
                TranslateRequest(currency.name, locale)
            )
        return (currency, errors)


class CurrencyService(CurrencyServiceInterface):

    def __init__(self, currency_repo: CurrencyRepo) -> None:
        self.currency_repo = currency_repo

    def get_currency(self,
                     key: str
                     ) -> Tuple[Optional[Currency], List[DomainError]]:
        return self.currency_repo.get(key)

    def get_all_currencies(self) -> List[Currency]:
        return self.currency_repo.get_all()
