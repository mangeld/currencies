from dataclasses import asdict

from flask import Blueprint, jsonify

from app.currencies.domain.interfaces import CurrencyNotFound
from app.currencies.factories import currency_service_factory

currencies_blueprint = Blueprint('currencies', __name__)


@currencies_blueprint.route('/currencies/', defaults={'currency_code': None})
@currencies_blueprint.route('/currencies/<currency_code>/')
def handle_get_currency(currency_code):
    # In an ideal case this would be managed by a DI framework.
    # In python we usually don't use DI frameworks but I've
    # used the symfony one before and I really enjoyed it.
    currency_service = currency_service_factory()

    if not currency_code:
        currencies = currency_service.get_all_currencies()
        return jsonify({
            'errors': [],
            'body': [asdict(currency) for currency in currencies],
            'pagination': {
                'next_url': None,
                'previous_url': None,
                'size': len(currencies)
            }
        }), 200

    currency, errors = currency_service.get_currency(currency_code)
    if CurrencyNotFound in errors:
        return jsonify({
            'errors': [asdict(error) for error in errors],
            'body': [],
            'pagination': {'next_url': None, 'previous_url': None, 'size': 0}
        }), 404

    return jsonify({
        'errors': [],
        'body': [asdict(currency)],
        'pagination': {'next_url': None, 'previous_url': None, 'size': 1}
    }), 200
