from typing import List, Optional, Tuple

from app.common.domain import DomainError
from app.currencies.domain.entities import Currency
from app.currencies.domain.interfaces import CurrencyNotFound, CurrencyRepo


# class CurrencyDBRepo(CurrencyRepo):
#     connection: object

#     def get(self, key: str) -> Tuple[Optional[Currency], List[DomainError]]:
#         data = self.connection.execute(
#             "SELECT * FROM currencies WHERE id=%s;",
#             key
#         )
#         if not data:
#             return (None, CurrencyNotFound())
#         return (Currency(
#             id=data.get('id'),
#             name=data.get('code')
#         ), [])


class DummyCurrencyRepo(CurrencyRepo):
    currencies = {
        'eur': Currency(id='eur', name='Fake currency'),
        'usd': Currency(id='usd', name='Fake usd currency')
    }

    def get(self, key: str) -> Tuple[Optional[Currency], List[DomainError]]:
        if key not in self.currencies:
            return None, [CurrencyNotFound()]
        return self.currencies[key], []

    def get_all(self) -> List[Currency]:
        return list(self.currencies.values())
