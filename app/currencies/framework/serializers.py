from marshmallow import Schema, fields


class Currency(Schema):
    pass


class CurrencyURL(Schema):
    id = fields.Str()
    link = fields.URL()
