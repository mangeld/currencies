
# How to run

```
docker-compose build app && docker-compose up app
```

By default it runs a development server with mocked data (doesn't have a db) on port 8080.

Since it doesn't have a db (it's all hard coded) updating entities isn't supported on the 
data access layer, but all the domain logic to be able to do that is implemented.

## Api endpoints

| Path | Parameters | Description | Supported Methods |
| ---- | ---------- | ----------- | ----------------- |
| `/rates/<currency_from>/<currency_to>/` | `currency_from`, `currency_to` | Returns an specific rate | GET, PUT, PUT, PATCH |
| `/rates/` | | Returns all rates | GET, POST |
| `/currencies/<currency>/` | `currency` | Returns the the detail of a currency | GET (POST not implemented)

## Architecture

The code is divided in folders that contain all the logic for a part of the domain,
for example the currencies folder has all the code related to the currencies entitites,
inside of those folders that separate contexts of the domain are subfolders called 'framework'
and 'domain' they solely purpose is to divide code in two layers, the framework layer that
has all the code that glues the application code with the exterior dependencies and the domain
layer wich has all the domain logic.

A good rule of thumb that I use to decide where code shoud go is:
    - If you're importing somthing that is not your own code, it has to go to the framework folder.
    - You cannot import code from the framework folders into the domain folders.

## Example requests

```
λ git master* → http GET localhost:8080/rates/eur/usd/
HTTP/1.0 200 OK
Content-Length: 201
Content-Type: application/json
Date: Mon, 19 Aug 2019 14:01:37 GMT
Server: Werkzeug/0.15.5 Python/3.7.2

[
    {
        "currencies": [
            {
                "id": "eur", 
                "link": "http://localhost:8080/currencies/eur/"
            }, 
            {
                "id": "usd", 
                "link": "http://localhost:8080/currencies/usd/"
            }
        ], 
        "last_updated": "2019-08-19T16:01:37.300855+00:00", 
        "rate": 1.11
    }
]
```

```
λ git master* → http GET localhost:8080/currencies/   
HTTP/1.0 200 OK
Content-Length: 159
Content-Type: application/json
Date: Mon, 19 Aug 2019 14:02:00 GMT
Server: Werkzeug/0.15.5 Python/3.7.2

{
    "body": [
        {
            "id": "eur", 
            "name": "Fake currency"
        }, 
        {
            "id": "usd", 
            "name": "Fake usd currency"
        }
    ], 
    "errors": [], 
    "pagination": {
        "next_url": null, 
        "previous_url": null, 
        "size": 2
    }
}
```
