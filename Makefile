.PHONY: lint hint pytest test

clean:
	find . -iname '*pyc' -delete

lint:
	python -m flake8 app

static-analysis:
	python -m mypy --ignore-missing-imports app

pytest:
	python -m pytest --cov=app -s -v app
	
test: clean lint static-analysis pytest
