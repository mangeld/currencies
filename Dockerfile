FROM python:3.7-alpine
LABEL MAINTAINER "Miguel Ángel Durán González <hi@mangel.me>"

WORKDIR /code

COPY requirements* /code/

RUN \
    apk add --no-cache --virtual=.build-deps build-base \
    && apk add --no-cache make \
    && pip install -r requirements_dev.txt \
    && apk del .build-deps

COPY entrypoint.sh /entrypoint.sh

COPY . /code/

ENV PYTHONPATH /code/app

ENTRYPOINT ["/entrypoint.sh"]

CMD ["serve"]
