#!/usr/bin/env sh

cd app

case $1 in
    'dev') flask run --host 0.0.0.0 --port 8080;;
    'serve') flask run --host 0.0.0.0 --port 8080;;
    'make' ) shift; cd .. ; make $@ ;;
    * ) exec $@ ;;
esac
